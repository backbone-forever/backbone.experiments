
const express = require('express')
const bodyParser = require("body-parser")
const shortid = require('shortid')

const app = express()

const port = process.env.PORT || 9090

app.use(express.static('public'))
app.use(express.json())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

let posts = []

app.get('/blogposts', (req, res) => {
  res.json(posts)
})

app.get('/blogposts/:id', (req, res) => {
  let id = req.params.id
  res.json(posts.find(item => item.id === id))
  // if err res.json(err)
})

app.post('/blogposts', (req, res) => {
  console.log("POST CREATE ", req.body);
  let model = req.body
  model.saveDate = new Date().valueOf()
  model.id = shortid.generate()
  posts.push(model)
  res.json(model)
  // if err res.json(err)
})

app.put('/blogposts/:id', (req, res) => {
  
  let id = req.params.id
  let model = req.body
  model.saveDate = new Date().valueOf()
  
  let position = posts.indexOf(posts.find(item=>item.id===id))
  posts.splice(position, 1, model)
  res.json(model)
  // if err res.json(err)
})

app.delete('/blogposts/:id', (req, res) => {
  let id = req.params.id
  let position = posts.indexOf(posts.find(item=>item.id===id))
  posts.splice(position, 1)
  res.json(id)
  // if err res.json(err)
})

app.get('/hello', (request, response) => {
  response.json({
    message: `hello bob`
  })
})

app.listen(port, () => {
  console.log(`👋 Jay the 🤖 is listening on port ${port} 😃`)
  console.log(`🌍 http://localhost:${port}`, "v0.0.0")
})