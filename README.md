# :construction: This is a work in progress

## Setup

```shell
npm install
```

## Run

```shell
npm start
# open http://localhost:9090
```

## If you want to rebuild `/public/web_modules`

> - you don't need to rebuild it for using the project
> - it's a packaged distribution of lit-element (and pwa-helper)

If for any reason you need to rebuild it:

- type `npx @pika/web --dest /public/web_modules`
- :warning: :wave: if you have an issue with files rights:
  - type `npx @pika/web`
  - and move `web_modules` to `/public`

