
- Backbone is a library
- LitElement is a library

### To read

- cash https://github.com/kenwheeler/cash
- https://github.com/jashkenas/backbone/wiki/Using-Backbone-without-jQuery
- https://www.inkling.com/blog/2013/05/leaving-jquery-behind-backbone-native/
- https://jbone.js.org/

### See

- https://gitlab.com/k33g/lit-simple
- https://gitlab.com/k33g/lit-bootstrap


### TODO

- BackboneSync for LocalStorage
- How to remove dependencies from backbone

