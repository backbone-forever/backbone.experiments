let Post = Backbone.Model.extend({})

let Posts = Backbone.Collection.extend({
  model: Post
})

let posts = new Posts

posts.add([
    new Post({ id:1, title: "Hello", message: "👋 Hello Word 🌍"})
  , new Post({ id:2, title: "Peace", message: "🖖 Peace and Prosperity 🌕"})
  , new Post({ id:3, title: "Leo", message: "the story of 🦁"})
  , new Post({ id:4, title: "Pandy", message: "the story of 🐼"})
])

export const postsCollection = posts
export const PostModel = Post
//export const PostsCollection = Posts

// for test in devtools
window.postsCollection = postsCollection
window.PostModel = PostModel