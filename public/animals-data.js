let AnimalBBModel = Backbone.Model.extend({})

let AnimalsBBCollection = Backbone.Collection.extend({
  model: AnimalBBModel
})

let animalsBBCollection = new AnimalsBBCollection

animalsBBCollection.add([
    new AnimalBBModel({ id:1, name: "Tiger", avatar: "🐯"})
  , new AnimalBBModel({ id:2, name: "Pandy", avatar: "🐼"})
  , new AnimalBBModel({ id:3, name: "Leo", avatar: "🦁"})
])

export const animalsCollection = animalsBBCollection
export const AnimalModel = AnimalBBModel
//export const AnimalCollection = AnimalsBBCollection

// for test in devtools
window.animalsCollection = animalsCollection
window.AnimalModel = AnimalModel