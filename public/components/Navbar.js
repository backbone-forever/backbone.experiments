import { LitElement, html } from '../web_modules/lit-element.js'

export class Navbar extends LitElement {
    
  static get styles() { return [window.picnicCssResult] }

  render() {
    return html`
      <div style="overflow: hidden;height: 250px;">
        <nav class="demo">
          <a href="#" class="brand">
            <span>BackBone + LitElement + Picnic</span>
          </a>
          <!-- responsive-->
          <input id="bmenub" type="checkbox" class="show">
          <label for="bmenub" class="burger pseudo button">🍔 menu</label>
  
          <div class="menu">
            <a href="#" class="pseudo button">one</a>
            <a href="#" class="button">two</a>
            <a href="#" class="button">three</a>
          </div>
        </nav>
      </div>
    `
  }
}

customElements.define('nav-bar', Navbar)