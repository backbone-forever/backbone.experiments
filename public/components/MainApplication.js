import { LitElement, html } from '../web_modules/lit-element.js'

import {} from './Navbar.js'
import {} from './BlogPosts.js'
import {} from './AnimalsList.js'

//import { installRouter } from '../web_modules/pwa-helpers.js'
import { postsCollection } from "../blog-posts-data.js"
import { animalsCollection } from "../animals-data.js"


export class MainApplication extends LitElement {

  static get styles() { return [window.picnicCssResult] }
  
  constructor() {
    super()
    
    let Router = Backbone.Router.extend({
      routes: {
        "posts/delete/:id_post": "deletePost",
        "animals/delete/:id_animal": "deleteAnimal",
        "*path": "root"
      },
      
      root: function() {
        console.log("🚀")
      },
  
      deletePost: (id_post) => {
        console.log(id_post)
        postsCollection.remove(postsCollection.get(id_post))
      },
      deleteAnimal: (id_animal) => {
        console.log("🖖", id_animal, animalsCollection.get(id_animal))
        animalsCollection.remove(animalsCollection.get(id_animal))
      }
    })
  
    new Router()
    Backbone.history.start()
    
    
    /*
    installRouter((location) => {
      console.log("🎃", location)
      
      if(location.hash.startsWith("#posts/delete/")) {
        let id = location.hash.split("/")[2]
        console.log(id)
        postsCollection.remove(postsCollection.get(id))
      }
  
      if(location.hash.startsWith("#animals/delete/")) {
        let id = location.hash.split("/")[2]
        console.log("🖖", id, animalsCollection.get(id))
        //animalsCollection.remove(animalsCollection.findWhere({name:name}))
        
        animalsCollection.remove(animalsCollection.get(id))
        // and remove the component of the list
  
      }
    })
    */
  }
  
  
  
  render() {
    return html`
      <nav-bar></nav-bar>
      <main style="margin:2%;">
        <div class="flex one two">
          <div class="content">
            <blog-posts></blog-posts>
          </div>
          <div class="content">
            <animals-list></animals-list>
          </div>
        </div>
      </main>
    `
  }
}

customElements.define('main-application', MainApplication)