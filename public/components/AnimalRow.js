import { LitElement, html } from '../web_modules/lit-element.js'

import { animalsCollection } from '../animals-data.js'

export class AnimalRow extends LitElement {
  static get styles() { return [window.picnicCssResult] }
  
  static get properties() {
    return {
      id: { attribute: 'animal-id'},
      name: { attribute: 'animal-name'},
  
      animal: { type: Object }
    }
  }
  
  constructor() {
    super()
  }
  connectedCallback() {
    super.connectedCallback()
    console.log("😃", animalsCollection.get(this.id))
    this.animal = animalsCollection.get(this.id).toJSON()
  
    animalsCollection.get(this.id).on("change", model => {
      this.animal = model.toJSON()
    })
  }
  

  render(){

    return html`
      <h3>${this.animal.id} ${this.animal.name} ${this.animal.avatar} <a href="/#animals/delete/${this.animal.id}">❎ remove</a></h3>
    `
  }
}
customElements.define('animal-row', AnimalRow)