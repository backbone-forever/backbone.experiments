import { LitElement, html } from '../web_modules/lit-element.js'

import {} from './AnimalRow.js'

import { animalsCollection, AnimalModel } from '../animals-data.js'


export class AnimalsList extends LitElement {
  static get styles() { return [window.picnicCssResult] }
  
  static get properties() {
    return {
      animals: { type: Array }
    }
  }
  
  constructor() {
    super()
    /* --- BackBone Stuff --- */
    
    this.animals = animalsCollection.toJSON()
  
    animalsCollection.on("add", (model) => {
      this.animals = [...this.animals, model.toJSON()]
      // check if it's "magic" /optimized or not with html``
    })
    
    animalsCollection.on("remove", (model) => {
      console.log("[AnimalsList]REMOVE", model, animalsCollection.toJSON())
      
      //Remove view from list: the collection is already updated
      let animalViewToRemove = this.shadowRoot.querySelector(`#animalrow_${model.get("id")}`)
      console.log(animalViewToRemove)
      animalViewToRemove.remove()
  
      //don't redraw all'
      //this.animals = this.animals.filter(animal => animal.id !== model.toJSON().id)
      
    })
  
    animalsCollection.on("change", (model) => {
      console.log("[AnimalsList]CHANGE", model)
      //don't redraw all'
      //this.animals = animalsCollection.toJSON()
    })
    
  }
  
  connectedCallback() {
    super.connectedCallback()
    
  }
  
  clickFrogHandler() {
    let froggy = new AnimalModel({id:4,name:"Froggy",avatar:"🐸"})
    animalsCollection.add(froggy)
    
  }
  
  render(){
    return html`
      <div class="card">
        <header><h3>Animals</h3></header>
        <section>
          ${this.animals.map(animal =>
            html`
                <animal-row id="animalrow_${animal.id}" animal-id=${animal.id} animal-name="${animal.name}"></animal-row>
            `
          )}
        </section>
        <footer>
           <button @click="${this.clickFrogHandler}">Add 🐸</button>
        <footer>
      </div>
    `
  }
}
customElements.define('animals-list', AnimalsList)
