import { LitElement, html } from '../web_modules/lit-element.js'

import { postsCollection } from '../blog-posts-data.js'

export class BlogPosts extends LitElement {
  static get styles() { return [window.picnicCssResult] }
  
  static get properties() {
    return {
      posts: { type: Array },
    }
  }
  
  constructor() {
    super()

    this.posts= postsCollection.toJSON()
    
    postsCollection.on("add", (model) => {
      this.posts = [...this.posts, model.toJSON()]
    })
    
    postsCollection.on("remove", (model) => {
      this.posts = this.posts.filter(post => post.id !== model.toJSON().id)
    })
  
    postsCollection.on("change", (model) => {
      this.posts = postsCollection.toJSON()
    })
    
  }
  
  render(){
    return html`
      <div class="card">
        <header><h3>BlogPosts</h3></header>
        <section>
          ${this.posts.map(post =>
            html`
              <h3>${post.title}: ${post.message} <a href="/#posts/delete/${post.id}">❎ remove</a></h3>
            `
          )}
        </section>
        <footer><footer>
      </div>
    `
  }
}
customElements.define('blog-posts', BlogPosts)
